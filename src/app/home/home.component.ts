import { Component, OnInit } from '@angular/core';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import { Label } from 'ng2-charts';
import { ServicosService } from '../shared/servicos.service';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  public barChartOptions: ChartOptions = {
    responsive: true,
    // We use these empty structures as placeholders for dynamic theming.
    scales: { xAxes: [{}], yAxes: [{}] },
  };
  public barChartLabels: Label[] = ['2006', '2007', '2008', '2009', '2010', '2011', '2012', '2013', '2014', '2015', '2016', '2017', '2018', '2019', '2020', '2021', '2022', '2023', '2024', '2025', '2026', '2027', '2028', '2029', '2030', '2031',];
  public barChartType: ChartType = 'bar';
  public barChartLegend = true;

  /*
  public barChartData: ChartDataSets[] = [
    { data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A' },
    { data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B' }
  ];
  */
  constructor(
    private servico: ServicosService
  ) { }

  public barChartData = [];

  ngOnInit() {
    this.servico.getDadosUsers()
      .pipe(
        map(resp => resp['results'])
      )
      .subscribe(res => {

        let item = res.filter(comp => comp['gender'] === 'female')
          .map(comp => {
            return  comp['dob'].age
          })
        
          let data = [];

          const items: ChartDataSets[] = [
            { data: item, label: 'Series A' },
            { data: item, label: 'Series B' }
          ]
        
        return this.barChartData = items;
        console.log('ponto', item.length)



      })
  }

 
  public controle(dados): ChartDataSets[]{
    return [{
      data: dados, label: 'Series A' 
    }]
  }

 // events
 public chartClicked({ event, active }: { event: MouseEvent, active: {}[] }): void {
  console.log(event, active);
}

public chartHovered({ event, active }: { event: MouseEvent, active: {}[] }): void {
  console.log(event, active);
}

public randomize(): void {
  this.barChartType = this.barChartType === 'bar' ? 'line' : 'bar';
}
}
