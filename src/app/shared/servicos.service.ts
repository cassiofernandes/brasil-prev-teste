import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { tap, map, filter } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ServicosService {

  private readonly API_URL = 'https://randomuser.me/api/?results=50&nat=br';

  constructor(
    private http: HttpClient
  ) { }

  getDadosUsers() {
    return this.http.get(this.API_URL)
  }

 
}
