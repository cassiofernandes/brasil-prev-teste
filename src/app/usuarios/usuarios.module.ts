import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsuariosRoutingModule } from './usuarios-routing.module';
import { UsuariosComponent } from './usuarios.component';
import { DetalhesUsuariosComponent } from './detalhes-usuarios/detalhes-usuarios.component';

@NgModule({
  declarations: [
    UsuariosComponent, 
    DetalhesUsuariosComponent
  ],
  imports: [
    CommonModule,
    UsuariosRoutingModule
  ]
})
export class UsuariosModule { }
