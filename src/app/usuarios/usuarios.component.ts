import { Component, OnInit } from '@angular/core';
import { ServicosService } from '../shared/servicos.service';
import { mapChildrenIntoArray } from '@angular/router/src/url_tree';
import { map, filter, tap } from 'rxjs/operators';

@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.css']
})
export class UsuariosComponent implements OnInit {

  usuarios: any;

  public lista: any;

  public emails: any;
  
  constructor(
    private servico: ServicosService
  ) { }
  
  ngOnInit() {
    this.servico.getDadosUsers()
      .pipe(
        map(res => res['results'])
      )
      .subscribe((resp: any) => {

        let emails = resp.map(lista => {
            return lista['email'];
          });
        let emailOrdem = emails.sort();
       
         this.emails = emailOrdem;
         console.log('emailOrdem', emailOrdem)   

        let dadoss = resp.map(item => {
         
            return item.name['first']+ ' '+item.name['last'];
            
          });

          let ordenado = dadoss.sort();



        return this.usuarios = ordenado;  
        }); 
    }
}
