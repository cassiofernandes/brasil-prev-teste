import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UsuariosComponent } from './usuarios.component';
import { DetalhesUsuariosComponent } from './detalhes-usuarios/detalhes-usuarios.component';

const routes: Routes = [
  { path: 'usuario', component: UsuariosComponent },
  { path: 'userdetalhe', component: DetalhesUsuariosComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsuariosRoutingModule { }
