import { Component, OnInit } from '@angular/core';
import { ServicosService } from 'src/app/shared/servicos.service';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-detalhes-usuarios',
  templateUrl: './detalhes-usuarios.component.html',
  styleUrls: ['./detalhes-usuarios.component.css']
})
export class DetalhesUsuariosComponent implements OnInit {

  usuarioDetalhes;

  constructor(
    private servico: ServicosService
  ) { }

  ngOnInit() {
    this.servico.getDadosUsers()
    .pipe(
      map(res => res['results'])
    )
    .subscribe((resp) => {
      console.log('Dados', resp);
      return this.usuarioDetalhes = resp;
    });
  }

}
